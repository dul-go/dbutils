# Database Utilities (dul-go)

## Installing to local Go environment

See this page:
https://edenmal.moe/post/2017/Golang-go-get-from-Gitlab/

```sh
git config --global url."git@gitlab.oit.duke.edu:".insteadOf "https://gitlab.oit.duke.edu/"

go get gitlab.oit.duke.edu/dul-go/dbutils
```

