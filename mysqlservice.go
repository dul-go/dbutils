package dbutils

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type MySqlService struct {
	db *sql.DB
}

func NewMySqlService(dataSource string) (*MySqlService, error) {
	// caller is responsible for closing db, and handling errors
	db, err := sql.Open("mysql", dataSource)
	return &MySqlService{db}, err
}

func (svc *MySqlService) Close() error {
	return svc.db.Close()
}
